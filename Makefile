.PHONY: proto

run:
	go run main.go run

build:
	mkdir -p dist/dist
	go build -o dist/dist/runner
	rsync -av db dist/dist/
	cd dist && tar cf dist.tar dist
	scp dist/dist.tar scukonick@srv1.scukonick.rocks:bot.tar

proto:
	protoc --go_out=plugins=grpc:proto/ *.proto
#	sed -i 's/json\(.*\),omitempty/json\1/' proto/*go

