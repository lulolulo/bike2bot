package kml_parser

import (
	"encoding/xml"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"unicode"

	"bitbucket.org/lulolulo/bike2bot/db"
)

var cdataRegexp = regexp.MustCompile(".*/><br><br>(.*)")

type Placemark struct {
	Name        string `xml:"name"`
	Description string `xml:"description"`
	Point       struct {
		Coordinates string `xml:"coordinates"`
		Location    struct {
			Lng float64
			Lat float64
		}
	}
}

type Folder struct {
	Name      string `xml:"name"`
	Placemark []Placemark
}

type Result struct {
	Document struct {
		Folder []Folder
	} `xml:"Document"`
}

func Parse(filename string) (*Result, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	res := &Result{}

	decoder := xml.NewDecoder(f)
	err = decoder.Decode(res)
	if err != nil {
		return nil, err
	}

	for i, folder := range res.Document.Folder {
		for j, placemark := range folder.Placemark {
			coordsStr := removeWhitespaces(placemark.Point.Coordinates)
			coordsArr := strings.Split(coordsStr, ",")
			if len(coordsArr) != 3 {
				return nil, fmt.Errorf("Invalid coords data: %v", coordsStr)
			}
			lng, err := strconv.ParseFloat(coordsArr[0], 64)
			if err != nil {
				return nil, fmt.Errorf("Invalid coords data: %v", coordsStr)
			}

			lat, err := strconv.ParseFloat(coordsArr[1], 64)
			if err != nil {
				return nil, fmt.Errorf("Invalid coords data: %v", coordsStr)
			}
			res.Document.Folder[i].Placemark[j].Point.Coordinates = coordsStr
			res.Document.Folder[i].Placemark[j].Point.Location.Lat = lat
			res.Document.Folder[i].Placemark[j].Point.Location.Lng = lng

			info := res.Document.Folder[i].Placemark[j].Description
			submatch := cdataRegexp.FindStringSubmatch(info)
			if len(submatch) != 2 {
				desc := strings.Replace(info, "<br>", "\n", -1)
				res.Document.Folder[i].Placemark[j].Description = desc
				continue
			}

			desc := strings.Replace(submatch[1], "<br>", "\n", -1)

			res.Document.Folder[i].Placemark[j].Description = desc
		}
	}

	return res, nil
}

func removeWhitespaces(input string) string {
	input = strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		return r
	}, input)

	return input
}

func PutInDB(result *Result, storage *db.Storage) {

}
