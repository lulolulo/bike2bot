package kml_parser

import "testing"

func TestParse(t *testing.T) {
	filename := "/tmp/input.kml"

	res, err := Parse(filename)
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("Res folders: %v", len(res.Document.Folder))

	f := res.Document.Folder[0]
	t.Logf("Name: %v", f.Name)

	for _, p := range f.Placemark {
		t.Logf("Placemark: %+v", p)
	}
}
