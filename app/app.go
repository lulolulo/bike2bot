package app

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/lulolulo/bike2bot/db"
	"bitbucket.org/lulolulo/bike2bot/db/structs"
	"github.com/Sirupsen/logrus"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

type UpdateProcessor func(c *UpdateContext)

type UpdateContext struct {
	update *tgbotapi.Update
	stop   bool
	bot    *tgbotapi.BotAPI
}

func (c *UpdateContext) Stop() {
	c.stop = true
}

type Server struct {
	storage    *db.Storage
	bot        *tgbotapi.BotAPI
	processors []UpdateProcessor
}

func NewServer(storage *db.Storage, bot *tgbotapi.BotAPI) *Server {
	return &Server{
		storage: storage,
		bot:     bot,
	}
}

func (s *Server) AddProcessor(p UpdateProcessor) {
	s.processors = append(s.processors, p)
}

func (s *Server) Run() error {
	logctx := logrus.WithField("action", "Run")
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	s.AddProcessor(s.updateUserInfo)
	s.AddProcessor(s.processStartMessage)
	s.AddProcessor(s.processLocationMessage)
	s.AddProcessor(s.processBonusQuery)
	s.AddProcessor(s.processInfoMessage)

	s.AddProcessor(s.processDefaultMessage)

	updates, err := s.bot.GetUpdatesChan(u)
	if err != nil {
		logctx.WithError(err).Error("Failed to get updates chan")
		return err
	}

	logctx.Info("AppServer started")
	for update := range updates {
		s.ProcessMessage(&update)
	}

	return nil
}

// ProcessMessage routes the message to the handler.
func (s *Server) ProcessMessage(update *tgbotapi.Update) {
	ctx := &UpdateContext{
		update: update,
		bot:    s.bot,
	}
	for _, p := range s.processors {
		p(ctx)
		if ctx.stop {
			break
		}
	}

	if update.InlineQuery != nil {
		q := update.InlineQuery
		logrus.WithField("data", q.Query).Info("Got inline query")
		return
	}
	if update.CallbackQuery != nil {
		q := update.CallbackQuery
		logrus.WithField("data", q.Data).Info("Got callback query")
		return
	}
}

func (s *Server) processBonusQuery(c *UpdateContext) {
	update := c.update
	if update.CallbackQuery == nil {
		return
	}
	q := update.CallbackQuery
	if !strings.HasPrefix(q.Data, "bonus:") {
		return
	}
	defer c.Stop()

	logctx := logrus.WithField("data", q.Data).WithField("tgID", q.From.ID).
		WithField("processor", "processBonusQuery")

	arr := strings.Split(q.Data, ":")
	if len(arr) != 2 {
		logctx.Error("Invalid data")
		return
	}

	bonusID, err := strconv.ParseInt(arr[1], 10, 32)
	if err != nil {
		logctx.WithError(err).Error("Invalid data")
		return
	}

	bonus, err := s.storage.GetBonusByID(int32(bonusID))
	if err != nil {
		logctx.WithError(err).Error("Failed to get bonus by id")
		return
	}

	chatID := q.Message.Chat.ID

	text := fmt.Sprintf("*%s*", bonus.Name)

	text += "\n\n"
	text += bonus.Description

	msg := tgbotapi.NewMessage(chatID, text)
	msg.ParseMode = "Markdown"

	_, err = s.bot.Send(msg)
	if err != nil {
		logctx.WithError(err).Error("Failed to send reply")
		return
	}

	loc := tgbotapi.NewLocation(chatID, bonus.Location.Lat, bonus.Location.Lng)
	_, err = s.bot.Send(loc)
	if err != nil {
		logctx.WithError(err).Error("Failed to send location")
		return
	}

	x := tgbotapi.NewCallback(q.ID, "Информация найдена")
	_, err = s.bot.AnswerCallbackQuery(x)
	if err != nil {
		logctx.WithError(err).Error("Failed to callback info")
		return
	}
}

// updateUserInfo updates user information in the database
func (s *Server) updateUserInfo(c *UpdateContext) {
	update := c.update
	if update.Message == nil {
		return
	}

	msg := update.Message
	tgUser := msg.From

	var username sql.NullString
	if tgUser.UserName != "" {
		username.String = tgUser.UserName
		username.Valid = true
	}

	user := &structs.User{
		FirstName: tgUser.FirstName,
		LastName:  tgUser.LastName,
		Username:  username,
		TgID:      int64(tgUser.ID),
	}

	_, err := s.storage.UpsertUserByTgID(user)
	if err != nil {
		logrus.WithError(err).Error("Failed to upsert user by tg id")
		return
	}
	return
}

func formatBonusesCountMessage(count, totalCount int) string {
	if count >= totalCount {
		if count == 1 {
			return "Вот, что я нашёл в радиусе 2 км от тебя (1 партнёр):"
		} else {
			return fmt.Sprintf("Вот, что я нашёл в радиусе 2 км от тебя (%d партнёра):", count)
		}
	}

	return "Вот, что я нашёл в радиусе 2 км от тебя (ближайшие 4 партнёра):"
}

// processLocationMessage gets location from message and
// looks up events from near it's location
// and sends them back to user
func (s *Server) processLocationMessage(c *UpdateContext) {
	if c.update.Message == nil {
		return
	}
	msg := c.update.Message
	if msg.Location == nil {
		return
	}
	defer c.Stop()

	loc := msg.Location
	x := loc.Longitude
	y := loc.Latitude

	logctx := logrus.WithField("lat", y).WithField("lng", x).
		WithField("processor", "processLocationMessage")

	user, err := s.storage.GetUserByTgID(int64(msg.From.ID))
	if err != nil {
		logctx.WithError(err).Error("Failed to get user by TG ID")
		return
	}

	_, err = s.storage.SaveLocation(user.ID, x, y)
	if err != nil {
		logctx.WithError(err).Error("Failed to save user location")
		return
	}

	bonusesNear, total, err := s.storage.GetBonusNear(x, y, 2000, 4)
	if err != nil {
		logctx.WithError(err).Error("Failed to get bonuses near")
		return
	}

	chatID := msg.Chat.ID

	if len(bonusesNear) == 0 {
		text := `К сожалению, в радиусе 2 км от тебя у акции нет партнёров.
Можешь посмотреть все предложения [на сайте](http://bike2work.ru/participate) или предложить любимому кафе [стать партнёром](http://bike2work.ru/partners/) в следующий раз.`
		reply := tgbotapi.NewMessage(chatID, text)
		reply.ParseMode = "Markdown"
		reply.DisableWebPagePreview = true
		_, err = s.bot.Send(reply)
		if err != nil {
			logctx.WithError(err).Error("Failed to send reply")
		}
		return
	}

	text := formatBonusesCountMessage(len(bonusesNear), total)
	reply := tgbotapi.NewMessage(chatID, text)
	_, err = s.bot.Send(reply)
	if err != nil {
		logctx.WithError(err).Error("Failed to send reply")
		return
	}

	time.Sleep(1 * time.Second)

	for _, bonus := range bonusesNear {
		text := fmt.Sprintf("*%s*", bonus.Name)

		text += "\n\n"
		text += bonus.Description

		msg := tgbotapi.NewMessage(chatID, text)
		msg.ParseMode = "Markdown"

		_, err = s.bot.Send(msg)
		if err != nil {
			logctx.WithError(err).Error("Failed to send reply")
			return
		}

		loc := tgbotapi.NewLocation(chatID, bonus.Location.Lat, bonus.Location.Lng)
		_, err = s.bot.Send(loc)
		if err != nil {
			logctx.WithError(err).Error("Failed to send location")
			return
		}
		time.Sleep(500 * time.Millisecond)
	}

	text = "Больше предложений на [сайте акции](http://bike2work.ru/participate)."
	reply = tgbotapi.NewMessage(chatID, text)
	reply.ParseMode = "Markdown"
	reply.DisableWebPagePreview = true
	reply.ReplyMarkup = getMarkup()

	_, err = s.bot.Send(reply)
	if err != nil {
		logctx.WithError(err).Error("Failed to send reply")
		return
	}
}

func (s *Server) processInfoMessage(c *UpdateContext) {
	if c.update.Message == nil {
		return
	}
	if c.update.Message.Text != "ℹ️ Инфо" {
		return
	}
	defer c.Stop()

	msg := c.update.Message

	resp := "Акция «На работу на велосипеде!» проходит в России два раза в год. " +
		"Она показывает, что ездить на велосипеде по ежедневным делам " +
		"может быть легко и удобно, для этого не требуется специальная " +
		"одежда или подготовка. Это не велопарад, привязанный " +
		"к определённому маршруту и времени, участники просто " +
		"едут по своим делам. Чтобы мотивировать новичков поехать " +
		"на работу на велосипеде впервые, организаторы акции и компании " +
		"устраивают конкурсы и делают в городе «энергетические точки», " +
		"партнёры акции предоставляют участникам скидки и бонусы."
	resp += "\n\n" +
		"• Принять [участие компанией](http://bike2work.ru/companies/)\n" +
		"• Стать [куратором акции](http://bike2work.ru/add-city) в своём городе\n" +
		"• Стать [партнёром](http://bike2work.ru/partners)\n" +
		"• Посмотреть все [скидки и бонусы](http://bike2work.ru/bonus) от партнёров\n"

	reply := tgbotapi.NewMessage(msg.Chat.ID, resp)
	reply.ParseMode = "Markdown"
	reply.DisableWebPagePreview = true
	reply.ReplyMarkup = getMarkup()

	_, err := s.bot.Send(reply)
	if err != nil {
		logrus.WithError(err).Error("Failed to send reply")
		return
	}
}

func getMarkup() tgbotapi.ReplyKeyboardMarkup {
	locButton := tgbotapi.NewKeyboardButtonLocation("📍 Показать бонусы рядом со мной")
	locRow := []tgbotapi.KeyboardButton{locButton}

	homeButton := tgbotapi.NewKeyboardButton("ℹ️ Инфо")
	homeRow := []tgbotapi.KeyboardButton{homeButton}

	markup := [][]tgbotapi.KeyboardButton{locRow, homeRow}
	return tgbotapi.ReplyKeyboardMarkup{
		Keyboard:       markup,
		ResizeKeyboard: true,
	}
}

func (s *Server) processStartMessage(c *UpdateContext) {
	if c.update.Message == nil {
		return
	}
	if c.update.Message.Text != "/start" {
		return
	}
	defer c.Stop()

	msg := c.update.Message
	response := "Привет, %v!\n\n" +
		"Я — официальный бот всероссийской акции " +
		"[«На работу на велосипеде»](http://bike2work.ru/bonus).\n" +
		"Отправь, пожалуйста, свои координаты, " +
		"я пришлю информацию о бонусах от партнёров акции, " +
		"расположенных рядом с тобой. Для этого сделай следующее: \n\n"

	resp2 := ` • Нажми 📎
 • Выбери "Location"
 • Нажми "Send my current location"
 • Или воспользуйся кнопкой внизу ⬇️`

	response += resp2

	response = fmt.Sprintf(response, msg.From.FirstName)

	reply := tgbotapi.NewMessage(msg.Chat.ID, response)
	reply.ParseMode = "Markdown"
	reply.DisableWebPagePreview = true

	reply.ReplyMarkup = getMarkup()

	_, err := s.bot.Send(reply)
	if err != nil {
		logrus.WithError(err).Error("Failed to send reply")
		return
	}

	return
}

func (s *Server) processDefaultMessage(c *UpdateContext) {
	if c.update.Message == nil {
		return
	}
	defer c.Stop()

	msg := c.update.Message

	response := "Я ещё только учусь, и не понимаю некоторые сообщения. " +
		"Отправь, пожаулйста, мне свои координаты, и я пришлю тебе " +
		"акции, расположенные рядом."

	user, err := s.storage.GetUserByTgID(int64(msg.From.ID))
	if err != nil {
		logrus.WithError(err).Error("Failed to find user by telegram ID")
		return
	}

	err = s.storage.CreateInvalidMessage(user.ID, msg.Text)
	if err != nil {
		logrus.WithError(err).Error("Failed to store invalid message")
		// not exiting here, need to send response
	}

	reply := tgbotapi.NewMessage(msg.Chat.ID, response)
	_, err = s.bot.Send(reply)
	if err != nil {
		logrus.WithError(err).Error("Failed to send response")
		return
	}
	return
}
