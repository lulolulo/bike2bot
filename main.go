package main

import (
	"os"

	"bitbucket.org/lulolulo/bike2bot/app"
	"bitbucket.org/lulolulo/bike2bot/cfg"
	"bitbucket.org/lulolulo/bike2bot/db"
	"bitbucket.org/lulolulo/bike2bot/db/structs"
	"bitbucket.org/lulolulo/bike2bot/kml_parser"
	log "github.com/Sirupsen/logrus"
	"github.com/codegangsta/cli"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

func cmdRun(c *cli.Context) {
	log.SetLevel(log.DebugLevel)

	config := cfg.GetConfig()

	bot, err := tgbotapi.NewBotAPI(config.BotToken)
	if err != nil {
		log.WithError(err).Fatal("Failed to create bot API")
	}
	bot.Debug = true

	dbConn, err := gorm.Open("postgres", config.DBConn)
	if err != nil {
		log.WithError(err).WithField("DBConn", config.DBConn).
			Fatal("Failed to open db connection")
	}

	storage := db.NewStorage(dbConn)

	appInstance := app.NewServer(storage, bot)

	err = appInstance.Run()
	if err != nil {
		log.WithError(err).Fatal("Failed to run app instance")
	}
}

func cmdParse(c *cli.Context) {
	filename := c.String("input")
	if filename == "" {
		log.Fatal("Invalid input")
	}

	city := c.String("city")
	if city == "" {
		log.Fatal("Invalid city")
	}

	config := cfg.GetConfig()

	dbConn, err := gorm.Open("postgres", config.DBConn)
	if err != nil {
		log.WithError(err).WithField("DBConn", config.DBConn).
			Fatal("Failed to open db connection")
	}
	storage := db.NewStorage(dbConn)

	res, err := kml_parser.Parse(filename)
	if err != nil {
		log.WithError(err).Fatal("Failed to parse file")
	}

	for _, folder := range res.Document.Folder {
		folderName := folder.Name
		for _, placemark := range folder.Placemark {
			name := placemark.Name
			desc := placemark.Description
			lng := placemark.Point.Location.Lng
			lat := placemark.Point.Location.Lat

			log.WithField("lat", lat).WithField("lng", lng).Info("Parsed point")

			bonus, err := storage.GetBonusByName(folderName, name, city)
			if err == db.ErrNotFound {
				bonus = &structs.Bonus{
					Folder:      folderName,
					Name:        name,
					Description: desc,
					Location: structs.GeoPoint{
						Lat: lat,
						Lng: lng,
					},
					City: city,
				}
				err = storage.UpsertBonus(bonus)
				if err != nil {
					log.WithError(err).Error("Failed to create bonus")
					continue
				}
			} else if err != nil {
				log.WithError(err).Error("Failed to lookup bonus in database")
				continue
			}

			bonus.Folder = folderName
			bonus.Name = name
			bonus.Description = desc
			bonus.Location.Lng = lng
			bonus.Location.Lat = lat

			err = storage.UpsertBonus(bonus)
			if err != nil {
				log.WithError(err).Error("Failed to create bonus")
				continue
			}
		}
	}
}

func main() {
	a := cli.NewApp()

	a.Version = "0.0.1"

	a.Commands = []cli.Command{
		{
			Name:   "run",
			Usage:  "Run backend process",
			Action: cmdRun,
		},
		{
			Name:   "parse",
			Usage:  "Run backend process",
			Action: cmdParse,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "input",
					Value: "input.kml",
					Usage: "kml file for parsing",
				},
				cli.StringFlag{
					Name:  "city",
					Value: "MSK",
					Usage: "map for which city",
				},
			},
		},
	}

	a.Run(os.Args)
}
