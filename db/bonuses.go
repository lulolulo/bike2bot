package db

import (
	"bitbucket.org/lulolulo/bike2bot/db/structs"
	"github.com/jinzhu/gorm"
)

func (s *Storage) GetBonusByName(folder, name, city string) (*structs.Bonus, error) {
	bonus := &structs.Bonus{}

	err := s.db.Where("folder = ?", folder).
		Where("name = ?", name).
		Where("city = ?", name).
		First(bonus).Error
	if err == gorm.ErrRecordNotFound {
		return nil, ErrNotFound
	} else if err != nil {
		return nil, err
	}

	return bonus, nil
}

func (s *Storage) UpsertBonus(bonus *structs.Bonus) error {
	var err error
	if bonus.ID == 0 {
		err = s.db.Create(bonus).Error
	} else {
		err = s.db.Save(bonus).Error
	}

	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) GetBonusNear(lng, lat float64, radius, limit int) ([]*structs.BonusNear, int, error) {

	query := `
	SELECT
	  bonuses.id,
	  bonuses.folder,
	  bonuses.name,
	  bonuses.description,
	  bonuses.location,
	  ST_Distance_Sphere(bonuses.location, ST_MakePoint(?, ?))::INTEGER as distance
	FROM bonuses
	WHERE ST_Distance_Sphere(bonuses.location, ST_MakePoint(?, ?)) <= ?
	ORDER BY distance ASC
	LIMIT ?
	`
	results := make([]*structs.BonusNear, 0, limit)
	err := s.db.Raw(query, lng, lat, lng, lat, radius, limit).Scan(&results).Error
	if err != nil {
		return nil, 0, err
	}

	count := &struct {
		Count int
	}{}
	query = `
	SELECT
	  COUNT(bonuses.id)
	FROM bonuses
	WHERE ST_Distance_Sphere(bonuses.location, ST_MakePoint(?, ?)) <= ?
	`

	err = s.db.Raw(query, lng, lat, radius).Scan(count).Error
	if err != nil {
		return nil, 0, err
	}

	return results, count.Count, err
}

func (s *Storage) GetBonusByID(bonusID int32) (*structs.Bonus, error) {
	bonus := &structs.Bonus{}
	err := s.db.Where("id = ?", bonusID).First(bonus).Error
	if err != nil {
		return nil, err
	}

	return bonus, err
}
