-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE bonuses (
  id          SERIAL PRIMARY KEY,
  folder      TEXT,
  name        TEXT,
  description TEXT,
  location    GEOMETRY(Point, 4326)
);

CREATE INDEX bonuses_location_idx
  ON bonuses (location);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE bonuses;