-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE "users" (
  id         SERIAL PRIMARY KEY,
  first_name TEXT,
  last_name  TEXT,
  username   TEXT,
  tg_id      BIGINT,
  created_at TIMESTAMP
);

CREATE UNIQUE INDEX users_tg_id_idx
  ON users (tg_id);
CREATE UNIQUE INDEX users_username_idx
  ON users (username);

CREATE TABLE "last_locations" (
  id         SERIAL PRIMARY KEY,
  user_id    INT REFERENCES users (id),
  location   GEOMETRY(Point, 4326),
  created_at TIMESTAMP
);

CREATE TABLE "invalid_messages" (
  id         SERIAL PRIMARY KEY,
  user_id    INT REFERENCES users (id),
  message    TEXT,
  created_at TIMESTAMP
);

CREATE INDEX last_locations_user_id_idx
  ON last_locations (user_id);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP TABLE "invalid_messages";
DROP TABLE "last_locations";
DROP TABLE "users";