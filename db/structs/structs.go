package structs

import (
	"database/sql"
	"time"
)

type User struct {
	ID        int32 `gorm:"primary_key"`
	FirstName string
	LastName  string
	Username  sql.NullString
	TgID      int64
	CreatedAt time.Time
}

type LastLocation struct {
	ID        int32 `gorm:"primary_key"`
	UserID    int32
	Location  GeoPoint
	CreatedAt time.Time
}

type InvalidMessage struct {
	ID        int32 `gorm:"primary_key"`
	UserID    int32
	Message   string
	CreatedAt time.Time
}

type Bonus struct {
	ID          int32 `gorm:"primary_key"`
	Folder      string
	Name        string
	Description string
	Location    GeoPoint
	City        string
}

func (Bonus) TableName() string {
	return "bonuses"
}

type BonusNear struct {
	ID          int32 `gorm:"primary_key"`
	Folder      string
	Name        string
	Description string
	Location    GeoPoint
	Distance    int32
}
