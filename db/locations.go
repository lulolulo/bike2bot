package db

import "bitbucket.org/lulolulo/bike2bot/db/structs"

func (s *Storage) SaveLocation(userID int32, lng, lat float64) (*structs.LastLocation, error) {
	lastLocation := &structs.LastLocation{
		UserID: userID,
		Location: structs.GeoPoint{
			Lat: lat,
			Lng: lng,
		},
	}

	err := s.db.Create(lastLocation).Error
	if err != nil {
		return nil, err
	}

	return lastLocation, nil
}
